package com.jsnoise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsNoiseApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsNoiseApplication.class, args);
	}
}
